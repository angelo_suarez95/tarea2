<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <?php
            $var = 24.5;
            echo $var;
            echo gettype($var), "\n";
            $var = "HOLA";
            echo $var;
            echo gettype($var), "\n";
            settype($var, 'interger'); 
            echo "var_dump: ";
            var_dump($var);
        ?>
    </body>
</html>